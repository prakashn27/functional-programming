(*fun fact_tail (0,ans) = ans
    | fact_tail(x,ans) = fact_tail (x-1, ans * x)
    *)

 (*fun fact x =
 	let fun help(0,ans) = ans
 	| help (x,ans) = help(x-1, ans * x)
 	in help(x, 1)
 	end
 *)
 (*fun f {x=0,y ,...} = y
	| f {x ,y=0,...} = x
	|f{x ,y ,z,q}=x*z+y*q
	*)

fun f x =
  let val a = x * 2
val b = x * 3 in
     0::x::a::b::(x*4)::[]
   end

fun len [] = 0 
| len (_::l) = 1 + len l
