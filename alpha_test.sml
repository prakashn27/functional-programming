(************************************)
(*******   alpha - Test Cases *******)
(************************************)

(******    Test Case - 1  ***********)
(* Lambda Equivalent ==> a = a  *)

val c1_t1  = V("a");
val c1_t2  = V("a");
val c1_ans = true;
val c1     = (c1_t1, c1_t2, c1_ans);

(******    Test Case - 2  ***********)
(*** Lambda Equivalent ==> b = b  ***)

val c2_t1  = V("b");
val c2_t2  = V("b");
val c2_ans = true;
val c2     = (c2_t1, c2_t2, c2_ans);

(******    Test Case - 3  ***********)
(*** Lambda Equivalent ==> Lx.x = Ly.y  ***)

val c3_t1  = L("x", V("x"));
val c3_t2  = L("y", V("y"));
val c3_ans = true;
val c3     = (c3_t1, c3_t2, c3_ans);

(******    Test Case - 4  ***********)
(*** Lambda Equivalent ==> (Lx.x  a) = (Ly.y  a)  ***)

val c4_t1  = A(L("x", V("x")), V("a"));
val c4_t2  = A(L("y", V("y")), V("a"));
val c4_ans = true;
val c4     = (c4_t1, c4_t2, c4_ans);

(******    Test Case - 5  ***********)
(*** Lambda Equivalent ==> (Lx.x  x) = (Ly.y  x)  ***)

val c5_t1  = A(L("x", V("x")), V("x"));
val c5_t2  = A(L("y", V("y")), V("x"));
val c5_ans = true;
val c5     = (c5_t1, c5_t2, c5_ans);

(******    Test Case - 6  ***********)
(*** Lambda Equivalent ==> Lf.Lx.(f  x)  =  Lx.Lf.(x  f)  ***)

val c6_t1  = L("f", L("x", A(V("f"), V("x"))));
val c6_t2  = L("x", L("f", A(V("x"), V("f"))));
val c6_ans = true;
val c6     = (c6_t1, c6_t2, c6_ans);

(******    Test Case - 7  ***********)
(*** Lambda Equivalent ==> Lf.Lx.(f (f x)) = Lg.Ly.(g (g y))  ***)

val c7_t1  = L("f", L("x", A(V("f") , A(V("f") , V("x") ))));
val c7_t2  = L("g", L("y", A(V("g") , A(V("g") , V("y") ))));
val c7_ans = true;
val c7     = (c7_t1, c7_t2, c7_ans);

(******    Test Case - 8  ***********)
(*** Lambda Equivalent ==> ((Lf.Lx.(f x)  a)  b) = ((Lg.Ly.(g y)  a)  b)  ***)

val c8_t1  = A(A(L("f", L("x", A(V("f"), V("x")))) , V("a")) , V("b"));
val c8_t2  = A(A(L("g", L("y", A(V("g"), V("y")))) , V("a")) , V("b"));
val c8_ans = true;
val c8     = (c8_t1, c8_t2, c8_ans);

(******    Test Case - 9  ***********)
(* Lambda Equivalent ==> a <> b  *)

val c9_t1  = V("a");
val c9_t2  = V("b");
val c9_ans = false;
val c9     = (c9_t1, c9_t2, c9_ans);

(******    Test Case - 10  ***********)
(*** Lambda Equivalent ==> Lx.x <> Ly.b  ***)

val c10_t1  = L("x", V("x"));
val c10_t2  = L("y", V("b"));
val c10_ans = false;
val c10     = (c10_t1, c10_t2, c10_ans);

(******    Test Case - 11  ***********)
(*** Lambda Equivalent ==> Lf.Lx.(f  x)  <>  Lx.Lf.(x  k)  ***)

val c11_t1  = L("f", L("x", A(V("f"), V("x"))));
val c11_t2  = L("x", L("f", A(V("x"), V("k"))));
val c11_ans = false;
val c11     = (c11_t1, c11_t2, c11_ans);

(******    Test Case - 12  ***********)
(*** Lambda Equivalent ==> Lf.Lx.(f  x)  <>  Lf.(x  k)  ***)

val c12_t1  = L("f", L("x", A(V("f"), V("x"))));
val c12_t2  = L("f", A(V("x"), V("k")));
val c12_ans = false;
val c12     = (c12_t1, c12_t2, c12_ans);

(******    Test Case - All  ***********)
val c_all  = [c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12];

fun  testAll (n, [ ]) =  "Passed all " ^ Int.toString(n-1) ^ " test cases"
  |  testAll (n, (t1, t2, ans) :: t) =
     if alpha(t1, t2) = ans
     then testAll(n+1,t)
     else "Failed at test case #" ^ Int.toString(n);
    
fun test () = testAll(1, c_all);
	 
