datatype 'a gametree = node of 'a * 'a gametree list; 

(*prune: 2 lines
treemap+tmap: 3 lines*)
fun s(p,glist) = [];

fun treemap s node(p, glist) = 	(* s is the strength func*)
	let fun tmap([]) = [] 
		| tmap(h:t) = s(p, h) :: tmap(t)
	in tmap(glist)
	end

fun prune 0 (node p glist) =  node p nil 
	| prune n (node p glist) = node p (map (prune n-1) glist);



(* fun prune 0 (node(p,glist)) = ... easy base case ....
    | prune n node(p,glist) = ... use the map function on lists and recurse on prune ... *)
(*
fun prune 0 node(p, (h::t)) = []
	| prune n node(p,(h::t)) = treemap(h) :: prune (n-1) node(p, t);
	*)







(*... use a helper function, say, tmap ... *)

	
