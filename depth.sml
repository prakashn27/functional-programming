datatype 'a tree  =  leaf  
		           | node of ‘a * 
				        'a tree  * 'a tree
fun depth(leaf(_)) = 1
   | depth(node(t1, t2)) =
           let val d1 = depth(t1);
               val d2 = depth(t2)
           in  if d1>d2 
               then 1+d1 
               else 1+d2
           end;